/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.healthbook;

import com.mycompany.healthbook.data.Dziecko;
import com.mycompany.healthbook.data.Rodzic;
import static java.lang.System.in;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bartek
 */
public class MySQLAccess {

    private Connection connect = null;
    private Statement statement = null;
    //private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private static MySQLAccess mySQLAccess = null;
    //użytkownik z pelnymi przywilejami + wrzucenie drivera mysql'a do folderu lib w tomcacies
    private MySQLAccess() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        connect = DriverManager.getConnection("jdbc:mysql://localhost/healthbook?" + "user=healthbook&password=healthbook");
        statement = connect.createStatement();
        System.out.println("połączono z bazą danych");
    }

    @Override
    protected void finalize () {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ex) {
                Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (getConnect() != null) {
            try {
                getConnect().close();
            } catch (SQLException ex) {
                Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static MySQLAccess pobierzInstancje() throws ClassNotFoundException, SQLException{
        if(mySQLAccess == null) {
            mySQLAccess = new MySQLAccess();
        }
        return mySQLAccess;
    }
    
    /**
     * @return the connect
     */
    public Connection getConnect() {
        return connect;
    }
    
//    public void dodajDoBazy(Dziecko dziecko){
//        // TO DO: dodanie do bazy
//    }
//    
//    public void dodajDoBazy(Rodzic rodzic){
//        // TO DO: dodanie do bazy
//    }
//    
//    public void usunZBazy(Integer pesel){
//        // TO DO: usunięcie z bazy
//    }
//    
//    public ArrayList<Object> pobierzZBazy(){
//        ArrayList<Object> lista = null;
//        //TO DO: pobranie z bazy
//        return lista;
//    }
    
//    public static void addDate(Wypozyczajacy user) throws SQLException {
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.wypozyczajacy values (?, ?, ?, ?, ?);");
//        preparedStatement.setInt(1, user.getNumer_karty());
//        preparedStatement.setString(2, user.getImie());
//        preparedStatement.setString(3, user.getNazwisko());
//        preparedStatement.setString(4, user.getAdres());
//        preparedStatement.setString(5, user.getHaslo());
//        preparedStatement.executeUpdate();
//    }
//
//    public static void addDate(Wypozyczenie w) throws SQLException {
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.wypozyczenie  values (?, ?, ?, ?, ?, ?);");
//        preparedStatement.setInt(1, w.getId_wypozyczenia());
//        preparedStatement.setDate(2, w.getData_wypozyczenia());
//        preparedStatement.setDate(3, w.getData_zwrotu());
//        preparedStatement.setInt(4, w.getNumer_karty_wypozyczajacego());
//        preparedStatement.setInt(5, w.getId_ksiazki());
//        preparedStatement.setBoolean(6, w.getStan());
//        preparedStatement.executeUpdate();
//    }
//
//    public static void addDate(Ksiazka k, List<Autor> a) throws SQLException {
//        connect.setAutoCommit(false);
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.ksiazki  values (?, ?, ?, ?, ?, ? );");
//        preparedStatement.setInt(1, k.getId_ksiazki());
//        preparedStatement.setString(2, k.getTytul());
//        preparedStatement.setLong(3, k.getISBN());
//        preparedStatement.setInt(4, k.getIlosc());
//        preparedStatement.setString(5, k.getWydawnictwo());
//        preparedStatement.setInt(6, k.getId_dziedziny());
//        preparedStatement.executeUpdate();
//        for (int i = 0; i < a.size(); ++i) {
//            addDate(new Autor_Ksiazka(k.getId_ksiazki(), a.get(i).getId_autora()));
//        }
//        connect.commit();
//        connect.setAutoCommit(true);
//    }
//
//    public static void addDate(Dziedzina d) throws SQLException {
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.dziedzina  values (?, ?);");
//        preparedStatement.setInt(1, d.getId_dziedziny());
//        preparedStatement.setString(2, d.getNazwa());
//        preparedStatement.executeUpdate();
//    }
//
//    public static void addDate(Autor_Ksiazka ak) throws SQLException {
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.autor_ksiazka  values (?, ?);");
//        preparedStatement.setInt(1, ak.getId_ksiazki());
//        preparedStatement.setInt(2, ak.getId_autora());
//        preparedStatement.executeUpdate();
//    }
//
//    public static void addDate(Autor a) throws SQLException {
//        preparedStatement = connect.prepareStatement("insert into  biblioteka.autorzy  values (?, ?, ? );");
//        preparedStatement.setInt(1, a.getId_autora());
//        preparedStatement.setString(2, a.getImie());
//        preparedStatement.setString(3, a.getNazwisko());
//        preparedStatement.executeUpdate();
//    }
//
//    public static void deleteKsiazka(Integer[] id) {
//        try {
//            connect.setAutoCommit(false);
//            for (int i = 0; i < id.length; ++i) {
//                statement.executeUpdate("DELETE FROM biblioteka.ksiazki WHERE ksiazki.ID_ksiazki = " + id[i].toString());
//            }
//            connect.commit();
//            connect.setAutoCommit(true);
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public static void deleteAutora(Integer[] id) {
//        try {
//            connect.setAutoCommit(false);
//            for (int i = 0; i < id.length; ++i) {
//                statement.executeUpdate("DELETE FROM biblioteka.autorzy WHERE ID_autora = " + id[i].toString());
//            }
//            connect.commit();
//            connect.setAutoCommit(true);
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    
//    public static void deleteDziedzine(Integer id) {
//        try {
//            statement.executeUpdate("DELETE FROM biblioteka.dziedzina WHERE ID_dziedziny = " + id.toString());
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    
//    public static void deleteAutorKsiazka(Integer idAutora, Integer idKsiazki) {
//        try {
//            statement.executeUpdate("DELETE FROM biblioteka.autor_ksiazka WHERE autor_ksiazka.ID_ksiazki = " + idKsiazki.toString()
//                    + " AND autor_ksiazka.ID_autora = " + idAutora.toString());
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public static ArrayList<Wypozyczajacy> readWypozyczajacy(String orderby) {
//        ArrayList<Wypozyczajacy> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String imie, nazwisko, adres, haslo;
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczajacy order by " + orderby);
//            while (resultSet.next()) {
//                id = Integer.parseInt(resultSet.getString("Numer_Karty"));
//                imie = resultSet.getString("imie");
//                nazwisko = resultSet.getString("nazwisko");
//                adres = resultSet.getString("adres");
//                haslo = resultSet.getString("haslo");
//                Wypozyczajacy nowy = new Wypozyczajacy(id, imie, nazwisko, adres, haslo);
//                lista.add(nowy);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//    
//    public static Wypozyczajacy readWypozyczajacy(Integer nrKarty, String haslo) {
//        //Integer id;
//        String imie, nazwisko, adres;
//        try {
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczajacy"
//                    + " where Numer_Karty ="+ nrKarty +" AND Haslo LIKE '"+ haslo +"';");
//            resultSet.next();
//            //id = Integer.parseInt(resultSet.getString("Numer_Karty"));
//            imie = resultSet.getString("Imie");
//            nazwisko = resultSet.getString("Nazwisko");
//            adres = resultSet.getString("Adres");
//            //haslo = resultSet.getString("haslo");
//            return new Wypozyczajacy(nrKarty, imie, nazwisko, adres, haslo);
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        } 
//        return null;
//    }
//    
//    public static ArrayList<Wypozyczajacy> readWypozyczajacy(String where, String like) {
//        ArrayList<Wypozyczajacy> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String imie, nazwisko, adres, haslo;
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczajacy where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id = Integer.parseInt(resultSet.getString("id"));
//                imie = resultSet.getString("imie");
//                nazwisko = resultSet.getString("nazwisko");
//                adres = resultSet.getString("adres");
//                haslo = resultSet.getString("haslo");
//                Wypozyczajacy nowy = new Wypozyczajacy(id, imie, nazwisko, adres, haslo);
//                lista.add(nowy);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Wypozyczenie> readWypozyczenie(String orderby) {
//        ArrayList<Wypozyczenie> lista = new ArrayList<>();
//        try {
//            Integer id;
//            Date data_wypozyczenia;
//            Date data_zwrotu;
//            Integer nr_karty_w;
//            Integer id_ksiazki;
//            Boolean stan;
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczenie order by " + orderby);
//            while (resultSet.next()) {
//                id = Integer.parseInt(resultSet.getString("ID_wypozyczenia"));
//                data_wypozyczenia = resultSet.getDate("Data_wypozyczenia");
//                data_zwrotu = resultSet.getDate("Data_zwrotu");
//                nr_karty_w = resultSet.getInt("Nr_karty_wypozyczajacego");
//                id_ksiazki = resultSet.getInt("ID_ksiazki");
//                stan = resultSet.getBoolean("Stan");
//                Wypozyczenie nowy = new Wypozyczenie(id, data_wypozyczenia, data_zwrotu, nr_karty_w, id_ksiazki, stan);
//                lista.add(nowy);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Wypozyczenie> readWypozyczenie(String where, String like) {
//        ArrayList<Wypozyczenie> lista = new ArrayList<>();
//        try {
//            Integer id;
//            Date data_wypozyczenia;
//            Date data_zwrotu;
//            Integer nr_karty_w;
//            Integer id_ksiazki;
//            Boolean stan;
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczenie where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id = Integer.parseInt(resultSet.getString("ID_wypozyczenia"));
//                data_wypozyczenia = resultSet.getDate("Data_wypozyczenia");
//                data_zwrotu = resultSet.getDate("Data_zwrotu");
//                nr_karty_w = resultSet.getInt("Nr_karty_wypozyczajacego");
//                id_ksiazki = resultSet.getInt("ID_ksiazki");
//                stan = resultSet.getBoolean("Stan");
//                Wypozyczenie nowy = new Wypozyczenie(id, data_wypozyczenia, data_zwrotu, nr_karty_w, id_ksiazki, stan);
//                lista.add(nowy);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readWypozyczenie(String where, String like, String orderBy, boolean kto) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Date data_wypozyczenia, data_zwrotu;
//            Integer nr_karty_w, id_ksiazki, ilosc;
//            Long ISBN;
//            String imieAutora, nazwiskoAutora, imieWyp, nazwiskoWyp, adresWyp,
//                    nazwaDziedz, tytul, wydawnictwo;
//            Boolean stan;
//            //Integer id_dziedziny;
//            resultSet = statement.executeQuery("SELECT * FROM biblioteka.wypozyczenie "
//                    + "JOIN biblioteka.ksiazki USING(ID_ksiazki) JOIN biblioteka.wypozyczajacy "
//                    + "on biblioteka.wypozyczenie.Nr_karty_wypozyczajacego = biblioteka.wypozyczajacy.Numer_Karty "
//                    + "JOIN biblioteka.autor_ksiazka USING ( ID_ksiazki ) JOIN "
//                    + "biblioteka.autorzy USING ( id_autora )  JOIN biblioteka.dziedzina "
//                    + "USING(id_dziedziny) where " + where + " like " + "'%" + like + "%' order by " + orderBy + ";");
//            while (resultSet.next()) {
//                ISBN = resultSet.getLong("ksiazki.ISBN");
//                adresWyp = resultSet.getString("wypozyczajacy.adres");
//                data_wypozyczenia = resultSet.getDate("wypozyczenie.Data_wypozyczenia");
//                data_zwrotu = resultSet.getDate("wypozyczenie.Data_zwrotu");
//                id_ksiazki = resultSet.getInt("wypozyczenie.ID_ksiazki");
//                ilosc = resultSet.getInt("ksiazki.Ilosc");
//                imieAutora = resultSet.getString("autorzy.Imie");
//                imieWyp = resultSet.getString("wypozyczajacy.Imie");
//                nazwaDziedz = resultSet.getString("dziedzina.Nazwa");
//                nazwiskoAutora = resultSet.getString("autorzy.Nazwisko");
//                nazwiskoWyp = resultSet.getString("wypozyczajacy.Nazwisko");
//                nr_karty_w = resultSet.getInt("wypozyczenie.Nr_karty_wypozyczajacego");
//                tytul = resultSet.getString("ksiazki.Tytul");
//                wydawnictwo = resultSet.getString("ksiazki.Wydawnictwo");
//                stan = resultSet.getBoolean("wypozyczenie.Stan");
//                String sStan;
//                if (stan) {
//                    sStan = "Zwrocona";
//                } else {
//                    sStan = "Wypozyczona";
//                }
//                Object[] obj;
//                if (kto) {
//                    obj = new Object[]{nr_karty_w, imieWyp, nazwiskoWyp, adresWyp,
//                        id_ksiazki, tytul, imieAutora, nazwiskoAutora, wydawnictwo,
//                        ISBN, nazwaDziedz, data_wypozyczenia, data_zwrotu, sStan
//                    };
//                } else {
//                    obj = new Object[]{id_ksiazki, tytul, imieAutora, nazwiskoAutora,
//                        wydawnictwo, ISBN, nazwaDziedz, data_wypozyczenia, data_zwrotu, sStan
//                    };
//                }
//                lista.add(obj);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Ksiazka> readKsiazka(String orderby) {
//        ArrayList<Ksiazka> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String tytul;
//            Long isbn;
//            Integer ilosc;
//            String wydawnictwo;
//            Integer id_dzieniny;
//            resultSet = statement.executeQuery("select * from biblioteka.ksiazki order by " + orderby);
//            while (resultSet.next()) {
//                id = resultSet.getInt("ID_ksiazki");
//                tytul = resultSet.getString("tytul");
//                isbn = resultSet.getLong("ISBN");
//                ilosc = resultSet.getInt("ilosc");
//                wydawnictwo = resultSet.getString("Wydawnictwo");
//                id_dzieniny = resultSet.getInt("ID_dziedziny");
//                Ksiazka nowa = new Ksiazka(id, tytul, isbn, ilosc, wydawnictwo, id_dzieniny);
//                lista.add(nowa);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readKsiazka(String where, String like) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String tytul;
//            Long isbn;
//            Integer ilosc;
//            String wydawnictwo;
//            Integer id_dzieniny;
//            resultSet = statement.executeQuery("select * from biblioteka.wypozyczajacy where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id = resultSet.getInt("ID_ksiazki");
//                tytul = resultSet.getString("tytul");
//                isbn = resultSet.getLong("ISBN");
//                ilosc = resultSet.getInt("ilosc");
//                wydawnictwo = resultSet.getString("Wydawnictwo");
//                id_dzieniny = resultSet.getInt("ID_dziedziny");
//                Ksiazka nowa = new Ksiazka(id, tytul, isbn, ilosc, wydawnictwo, id_dzieniny);
//                lista.add(new Object[]{id, tytul, isbn, ilosc, wydawnictwo, id_dzieniny});
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    //@SuppressWarnings("empty-statement")
//    public static ArrayList<Object[]> readKsiazka(String where, String like, String orderBy, boolean kto) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id_autora;
//            String imie;
//            String nazwisko;
//            Integer id_dziedziny;
//            String nazwa;
//            Integer id_ksiazki;
//            String tytul;
//            Long ISBN;
//            Integer ilosc;
//            String wydawnictwo;
//            int i = 0;
//            resultSet = statement.executeQuery("SELECT * FROM biblioteka.ksiazki "
//                    + "JOIN biblioteka.autor_ksiazka USING ( ID_ksiazki ) JOIN "
//                    + "biblioteka.autorzy USING ( id_autora )  JOIN biblioteka.dziedzina "
//                    + "USING(id_dziedziny) where " + where + " like " + "'%" + like + "%' order by " + orderBy + ";");
//            while (resultSet.next()) {
//                id_dziedziny = resultSet.getInt("ID_dziedziny");
//                id_autora = resultSet.getInt("ID_autora");
//                id_ksiazki = resultSet.getInt("ID_ksiazki");
//                tytul = resultSet.getString("tytul");
//                ISBN = resultSet.getLong("ISBN");
//                ilosc = resultSet.getInt("ilosc");
//                wydawnictwo = resultSet.getString("Wydawnictwo");
//                imie = resultSet.getString("Imie");
//                nazwisko = resultSet.getString("Nazwisko");
//                nazwa = resultSet.getString("Nazwa");
//                /*lista.add(id_ksiazki.toString() + " " + tytul + " " + ISBN.toString()
//                 + " " + ilosc.toString() + " " + wydawnictwo + " " + imie + " " 
//                 + nazwisko + " " + nazwa);*/
//                Object lista1[];
//                if(kto){
//                lista1 = new Object[]{id_ksiazki, tytul, id_autora, imie, nazwisko, wydawnictwo, ISBN, ilosc, id_dziedziny, nazwa};
//                }else{
//                    lista1 = new Object[]{id_ksiazki, tytul, imie, nazwisko, wydawnictwo, ISBN, ilosc, nazwa};
//                }
//                lista.add(lista1);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readDziedzina(String orderby) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String nazwa;
//            resultSet = statement.executeQuery("select * from biblioteka.dziedzina order by " + orderby);
//            while (resultSet.next()) {
//                id = resultSet.getInt("ID_dziedziny");
//                nazwa = resultSet.getString("Nazwa");
//                Dziedzina nowa = new Dziedzina(id, nazwa);
//                lista.add(new Object[]{id, nazwa});
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readDziedzina(String where, String like) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id;
//            String nazwa;
//            resultSet = statement.executeQuery("select * from biblioteka.dziedzina where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id = resultSet.getInt("ID_dzieniny");
//                nazwa = resultSet.getString("Nazwa");
//                Dziedzina nowa = new Dziedzina(id, nazwa);
//                lista.add(new Object[]{id, nazwa});
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Autor_Ksiazka> readAutor_Ksiazka(String orderby) {
//        ArrayList<Autor_Ksiazka> lista = new ArrayList<>();
//        try {
//            Integer id_ksiazki;
//            Integer id_autora;
//            resultSet = statement.executeQuery("select * from biblioteka.autor_ksiazka order by " + orderby);
//            while (resultSet.next()) {
//                id_ksiazki = resultSet.getInt("ID_ksiazki");
//                id_autora = resultSet.getInt("tytul");
//                Autor_Ksiazka nowa = new Autor_Ksiazka(id_ksiazki, id_autora);
//                lista.add(nowa);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Autor_Ksiazka> readAutor_Ksiazka(String where, String like) {
//        ArrayList<Autor_Ksiazka> lista = new ArrayList<>();
//        try {
//            Integer id_ksiazki;
//            Integer id_autora;
//            resultSet = statement.executeQuery("select * from biblioteka.autor_ksiazka where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id_ksiazki = resultSet.getInt("ID_ksiazki");
//                id_autora = resultSet.getInt("tytul");
//                Autor_Ksiazka nowa = new Autor_Ksiazka(id_ksiazki, id_autora);
//                lista.add(nowa);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readAutor(String orderby) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id_autora;
//            String imie;
//            String nazwisko;
//            resultSet = statement.executeQuery("select * from biblioteka.autorzy order by " + orderby);
//            while (resultSet.next()) {
//                id_autora = resultSet.getInt("ID_autora");
//                imie = resultSet.getString(2);
//                nazwisko = resultSet.getString(3);
//                Autor nowa = new Autor(id_autora, imie, nazwisko);
//                lista.add(new Object[]{id_autora, imie, nazwisko});
//            }
//        } catch (SQLException | NullPointerException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static ArrayList<Object[]> readAutor(String where, String like) {
//        ArrayList<Object[]> lista = new ArrayList<>();
//        try {
//            Integer id_autora;
//            String imie;
//            String nazwisko;
//            resultSet = statement.executeQuery("select * from biblioteka.autor where " + where + " like " + "'%" + like + "%';");
//            while (resultSet.next()) {
//                id_autora = resultSet.getInt("ID_autora");
//                imie = resultSet.getString("Imie");
//                nazwisko = resultSet.getString("Nazwisko");
//                Autor nowa = new Autor(id_autora, imie, nazwisko);
//                lista.add(new Object[]{id_autora, imie, nazwisko});
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MySQLAccess.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return lista;
//    }
//
//    public static void update(String what, String set, String where) throws SQLException {
//        statement.executeUpdate("UPDATE " + what
//                + " SET " + set + " WHERE " + where + ";");
//    }
//
//    public static void updateWypozyczenie(List<Integer[]> idKsiazki) throws SQLException {
//        connect.setAutoCommit(false);
//        for (int i = 0; i < idKsiazki.size(); ++i) {
//            statement.executeUpdate("UPDATE biblioteka.ksiazki"
//                    + " SET Ilosc = Ilosc + 1 WHERE ID_ksiazki = " + idKsiazki.get(i)[0].toString() + ";");
//            statement.executeUpdate("UPDATE biblioteka.wypozyczenie"
//                    + " SET Stan = 1 WHERE ID_wypozyczenia = " + idKsiazki.get(i)[1].toString() + ";");
//        }
//        connect.commit();
//        connect.setAutoCommit(true);
//    }
//    
//    public static void deleteWypozyczenie(List<Integer[]> idKsiazki) throws SQLException {
//        connect.setAutoCommit(false);
//        for (int i = 0; i < idKsiazki.size(); ++i) {
//            statement.executeUpdate("UPDATE biblioteka.ksiazki"
//                    + " SET Ilosc = Ilosc + 1 WHERE ID_ksiazki = " + idKsiazki.get(i)[0].toString() + ";");
//            statement.executeUpdate("DELETE FROM biblioteka.wypozyczenie"
//                    + " WHERE ID_wypozyczenia = " + idKsiazki.get(i)[1].toString() + ";");
//        }
//        connect.commit();
//        connect.setAutoCommit(true);
//    }

}
