/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.healthbook.data;

import java.sql.Date;

/**
 *
 * @author And
 */
public class Dziecko extends Osoba {
    private Date dataUrodzenia;
    private String miejsceUrodzenia;
    private String grupaKrwi;
    private Integer idRodzice;
    
    public Dziecko(Integer pesel, String imie, String nazwisko, String adres, 
            Date dataUrodzenia, String miejsceUrodzenia, String grupaKrwi, Integer idRodzica){
        super(pesel, imie, nazwisko, adres);
        this.dataUrodzenia = dataUrodzenia;
        this.miejsceUrodzenia = miejsceUrodzenia;
        this.grupaKrwi = grupaKrwi;
        this.idRodzice = idRodzica;
    }

    /**
     * @return the DataUrodzenia
     */
    public Date getDataUrodzenia() {
        return dataUrodzenia;
    }

    /**
     * @param DataUrodzenia the DataUrodzenia to set
     */
    public void setDataUrodzenia(Date DataUrodzenia) {
        this.dataUrodzenia = DataUrodzenia;
    }

    /**
     * @return the MiejsceUrodzenia
     */
    public String getMiejsceUrodzenia() {
        return miejsceUrodzenia;
    }

    /**
     * @param MiejsceUrodzenia the MiejsceUrodzenia to set
     */
    public void setMiejsceUrodzenia(String MiejsceUrodzenia) {
        this.miejsceUrodzenia = MiejsceUrodzenia;
    }

    /**
     * @return the GrupaKrwi
     */
    public String getGrupaKrwi() {
        return grupaKrwi;
    }

    /**
     * @param GrupaKrwi the GrupaKrwi to set
     */
    public void setGrupaKrwi(String GrupaKrwi) {
        this.grupaKrwi = GrupaKrwi;
    }

    /**
     * @return the Idrodzice
     */
    public Integer getIdrodzice() {
        return idRodzice;
    }

    /**
     * @param Idrodzice the Idrodzice to set
     */
    public void setIdrodzice(Integer Idrodzice) {
        this.idRodzice = Idrodzice;
    }
}
