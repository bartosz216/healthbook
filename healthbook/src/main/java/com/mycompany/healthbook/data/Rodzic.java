/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package com.mycompany.healthbook.data;

import java.sql.Date;

/**
*
* @author And
*/
public class Rodzic extends Osoba{
    private String miejsceUrodzenia;
    private Date dataUrodzenia;
    private String grupaKrwi;

    public Rodzic(Integer pesel, String imie, String nazwisko, String adres){
        super(pesel,imie,nazwisko, adres);
    }
    /**
* @return the MiejsceUrodzenia
*/
    public String getMiejsceUrodzenia() {
        return miejsceUrodzenia;
    }

    /**
* @param MiejsceUrodzenia the MiejsceUrodzenia to set
*/
    public void setMiejsceUrodzenia(String MiejsceUrodzenia) {
        this.miejsceUrodzenia = MiejsceUrodzenia;
    }

    /**
* @return the DataUrodzenia
*/
    public Date getDataUrodzenia() {
        return dataUrodzenia;
    }

    /**
* @param DataUrodzenia the DataUrodzenia to set
*/
    public void setDataUrodzenia(Date DataUrodzenia) {
        this.dataUrodzenia = DataUrodzenia;
    }

    /**
* @return the GrupaKrwi
*/
    public String getGrupaKrwi() {
        return grupaKrwi;
    }

    /**
* @param GrupaKrwi the GrupaKrwi to set
*/
    public void setGrupaKrwi(String GrupaKrwi) {
        this.grupaKrwi = GrupaKrwi;
    }
}