/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.healthbook.data;

/**
 *
 * @author And
 */
public class Osoba {
    protected Integer pesel;
    protected String imie;
    protected String nazwisko;
    protected String adres;

    public Osoba(Integer pesel, String imie, String nazwisko, String adres){
        this.pesel = pesel;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
    }

    /**
     * @return the Pesel
     */
    public Integer getPesel() {
        return pesel;
    }

    /**
     * @param Pesel the Pesel to set
     */
    public void setPesel(Integer Pesel) {
        this.pesel = Pesel;
    }

    /**
     * @return the Imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param Imie the Imie to set
     */
    public void setImie(String Imie) {
        this.imie = Imie;
    }

    /**
     * @return the Nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @param Nazwisko the Nazwisko to set
     */
    public void setNazwisko(String Nazwisko) {
        this.nazwisko = Nazwisko;
    }

    /**
     * @return the Adres
     */
    public String getAdres() {
        return adres;
    }

    /**
     * @param Adres the Adres to set
     */
    public void setAdres(String Adres) {
        this.adres = Adres;
    }
}
