/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.healthbook.view;

import com.mycompany.healthbook.MyVaadinUI;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.Iterator;

/**
 *
 * @author And
 */
public class MainView extends HorizontalLayout implements View{
    protected Panel content;
    protected CssLayout menu = new CssLayout();

    // Menu navigation button listener
    class ButtonListener implements Button.ClickListener {

        String menuitem;

        public ButtonListener(String menuitem) {
            this.menuitem = menuitem;
        }

        @Override
        public void buttonClick(ClickEvent event) {
            // Navigate to a specific state
            clearMenuSelection();
            //navigator.navigateTo(MAINVIEW + "/" + menuitem);
            event.getButton().addStyleName("selected");
        }
    }

    public MainView() {
        setSizeFull();

        addComponent(new VerticalLayout() {
            //całe boczne menu
            {
                addStyleName("sidebar");
                setWidth(null);
                setHeight("100%");

                //logo
                addComponent(new CssLayout() {
                    {
                        addStyleName("branding");
                        Label logo = new Label(
                                "Elektroniczna</br>książeczka</br>zdrowia",
                                ContentMode.HTML);
                        logo.setSizeUndefined();
                        addComponent(logo);
                    }
                });
                
                //menu
                addComponent(menu);
                setExpandRatio(menu, 1);

                //menu użytkownika 
                addComponent(new VerticalLayout() {
                    {
                        setSizeUndefined();
                        addStyleName("user");
                        Image profilePic = new Image(
                                null,
                                new ThemeResource("img/profile-pic.png"));
                        profilePic.setWidth("34px");
                        addComponent(profilePic);
                        Label userName = new Label("tatar");
                        userName.setSizeUndefined();
                        addComponent(userName);

                        MenuBar.Command cmd = new MenuBar.Command() {
                            @Override
                            public void menuSelected(
                                    MenuBar.MenuItem selectedItem) {
                                Notification
                                        .show("Not implemented in this demo");
                            }
                        };
                        MenuBar settings = new MenuBar();
                        MenuBar.MenuItem settingsMenu = settings.addItem("Menu",
                                null);
                        settingsMenu.addItem("Settings", cmd);
                        settingsMenu.addItem("Preferences", cmd);
                        settingsMenu.addSeparator();
                        settingsMenu.addItem("My Account", cmd);
                        addComponent(settings);

                        Button exit = new NativeButton("Wyloguj");
                        addComponent(exit);
                        exit.addClickListener(new Button.ClickListener() {
                            @Override
                            public void buttonClick(ClickEvent event) {
                                //navigator.navigateTo("");
                            }
                        });
                    }
                });
            }
        });

        menu.addComponent(new NativeButton("Pigsads adasdasd asdasdasda sd",
                new ButtonListener("pig")));
        menu.addComponent(new NativeButton("Cat",
                new ButtonListener("cat")));
        menu.addComponent(new NativeButton("Dog",
                new ButtonListener("dog")));
        menu.addComponent(new NativeButton("Reindeer",
                new ButtonListener("reindeer")));
        menu.addComponent(new NativeButton("Penguin",
                new ButtonListener("penguin")));
        menu.addComponent(new NativeButton("Sheep",
                new ButtonListener("sheep")));
        menu.addStyleName("menu");

        content = new Panel();
        content.setSizeFull();
        content.setStyleName("borderless");
        addComponent(content);
        setExpandRatio(content, 1);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        VerticalLayout panelContent = new VerticalLayout();
        panelContent.setSizeFull();
        panelContent.setMargin(true);
        content.setContent(panelContent); // Also clears

        if (event.getParameters() == null
                || event.getParameters().isEmpty()) {
            panelContent.addComponent(
                    new Label("Nothing to see here, "
                            + "just pass along."));
            return;
        }

        // Display the fragment parameters
        Label watching = new Label(
                "You are currently watching a "
                + event.getParameters());
        watching.setSizeUndefined();
        panelContent.addComponent(watching);
        panelContent.setComponentAlignment(watching,
                Alignment.MIDDLE_CENTER);

        // Some other content
        Embedded pic = new Embedded(null,
                new ThemeResource("img/" + event.getParameters()
                        + "-128px.png"));
        panelContent.addComponent(pic);
        panelContent.setExpandRatio(pic, 1.0f);
        panelContent.setComponentAlignment(pic,
                Alignment.MIDDLE_CENTER);

        Label back = new Label("And the "
                + event.getParameters() + " is watching you");
        back.setSizeUndefined();
        panelContent.addComponent(back);
        panelContent.setComponentAlignment(back,
                Alignment.MIDDLE_CENTER);
    }

    protected void clearMenuSelection() {
        for (Iterator<Component> it = menu.getComponentIterator(); it.hasNext();) {
            Component next = it.next();
            if (next instanceof NativeButton) {
                next.removeStyleName("selected");
            } else if (next instanceof DragAndDropWrapper) {
                // Wow, this is ugly (even uglier than the rest of the code)
                ((DragAndDropWrapper) next).iterator().next()
                        .removeStyleName("selected");
            }
        }
    }

}
