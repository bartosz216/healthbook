/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.healthbook.view;

import com.mycompany.healthbook.data.doubleString;

/**
 *
 * @author And
 */
public interface interfaceView {
        public void addListener(interfaceViewListener lis);
        public doubleString getLoginAndPass();
        public interface interfaceViewListener {
		void buttonClick(String oper);
	}
    
}
