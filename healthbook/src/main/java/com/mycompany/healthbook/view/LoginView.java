/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.healthbook.view;

import com.mycompany.healthbook.data.doubleString;
import com.mycompany.healthbook.model.loginSQLModel;
import com.mycompany.healthbook.presenter.loginPresenter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author And
 */
public class LoginView extends VerticalLayout implements interfaceView, Button.ClickListener,View{
    private List<interfaceViewListener> listeners = new ArrayList<interfaceViewListener>();
    private VerticalLayout layout=new VerticalLayout();
    private TextField loginField = new TextField();
    private TextField passField = new TextField();
    private Button checkPass = new Button("log in");
    
    
    public LoginView(){
        setSizeFull();
        initLayout();
        loginSQLModel model = new loginSQLModel();
        loginPresenter presenter = new loginPresenter(this, model);
    }
    public void initLayout() {	
	addComponent(loginField);
        addComponent(passField);
        addComponent(checkPass);
        checkPass.addClickListener(this);
	
	}
    public VerticalLayout getLayuot() {
	return layout;
    }
   
    public void addListener(interfaceViewListener lis){
		listeners.add(lis);
	}
    
    public doubleString getInfo(){
        return new doubleString(loginField.getValue(), passField.getValue());
    }
    
    public doubleString getLoginAndPass(){
        return new doubleString(loginField.getValue(), passField.getValue());
    }
    
    
    @Override
    public void buttonClick(ClickEvent event) {
        System.out.println("wysyłam do prezentera");
        for (interfaceViewListener listener : listeners){
            listener.buttonClick("check");
	}
		
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         Notification.show("login form");
    }
}
