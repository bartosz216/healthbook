package com.mycompany.healthbook;

import com.mycompany.healthbook.model.loginSQLModel;
import com.mycompany.healthbook.presenter.loginPresenter;
import com.mycompany.healthbook.view.LoginView;
import com.mycompany.healthbook.view.MainView;
import com.mycompany.healthbook.view.TestView;
import com.sun.swing.internal.plaf.metal.resources.metal;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI
{
    Navigator navigator;
    protected static final String MAINVIEW = "main";
    
    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "com.mycompany.healthbook.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("test");
        navigator = new Navigator(this, this);

        navigator.addView("", new LoginView());
        navigator.addView(MAINVIEW, new MainView());
        navigator.addView(MAINVIEW+"/test", new TestView());
        System.out.println("int navigatora");
        MySQLAccess poloczenie;
        try {
            poloczenie = MySQLAccess.pobierzInstancje();
            Statement statement = poloczenie.getConnect().createStatement();
            ResultSet resultSet = statement.executeQuery("select * from healthbook.dziecko");
                    
            resultSet.next();
            System.out.println(resultSet.getString("imie"));
            //final VerticalLayout layout = new VerticalLayout();
            //layout.setMargin(true);
            //setContent(layout);
            
            //Button button = new Button("Click Me");
            //button.addClickListener(new Button.ClickListener() {
            //    public void buttonClick(ClickEvent event) {
            //        layout.addComponent(new Label("Thank you for clicking2"));
            //    }
            //});
            //layout.addComponent(button);
            
            //LoginView login = new LoginView();
            //loginSQLModel model = new loginSQLModel();
            //loginPresenter presenter = new loginPresenter(login, model);
            
            //setContent(login.getLayuot());
            //setContent(login.getLayuot());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MyVaadinUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MyVaadinUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public Navigator getNavigator(){
        return navigator;
    }

}
