-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Maj 2014, 15:46
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `healthbook`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dzieci`
--

CREATE TABLE IF NOT EXISTS `dzieci` (
  `pesel` int(11) NOT NULL,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(30) NOT NULL,
  `data_urodzenia` date NOT NULL,
  `miejsce_urodzenia` varchar(40) NOT NULL,
  `adres` varchar(40) NOT NULL,
  `grupa_krwi` varchar(5) NOT NULL,
  `id_rodzice` int(11) NOT NULL,
  PRIMARY KEY (`pesel`),
  KEY `id_rodzice` (`id_rodzice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarze`
--

CREATE TABLE IF NOT EXISTS `lekarze` (
  `pesel` int(11) NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(40) NOT NULL,
  `data_zatrudnienia` date NOT NULL,
  `adres` varchar(40) NOT NULL,
  `stanowisko` varchar(40) NOT NULL,
  `haslo` varchar(40) NOT NULL,
  PRIMARY KEY (`pesel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rodzice`
--

CREATE TABLE IF NOT EXISTS `rodzice` (
  `pesel` int(11) NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(40) NOT NULL,
  `data` date NOT NULL,
  `miejsce_urodzenia` varchar(40) NOT NULL,
  `adres` varchar(40) NOT NULL,
  `grupa_krwi` varchar(5) NOT NULL,
  PRIMARY KEY (`pesel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wpis`
--

CREATE TABLE IF NOT EXISTS `wpis` (
  `lekarz` int(11) NOT NULL,
  `dziecko` int(11) NOT NULL,
  `data` date NOT NULL,
  `treść` text NOT NULL,
  KEY `lekarz` (`lekarz`,`dziecko`),
  KEY `dziecko` (`dziecko`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `dzieci`
--
ALTER TABLE `dzieci`
  ADD CONSTRAINT `dzieci_ibfk_1` FOREIGN KEY (`id_rodzice`) REFERENCES `rodzice` (`pesel`);

--
-- Ograniczenia dla tabeli `wpis`
--
ALTER TABLE `wpis`
  ADD CONSTRAINT `wpis_ibfk_2` FOREIGN KEY (`lekarz`) REFERENCES `lekarze` (`pesel`),
  ADD CONSTRAINT `wpis_ibfk_1` FOREIGN KEY (`dziecko`) REFERENCES `dzieci` (`pesel`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

