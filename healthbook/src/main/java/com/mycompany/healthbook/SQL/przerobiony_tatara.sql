
CREATE TABLE Badania_okulistyczne (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Wynik_badania varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Badania_okulistyczne_FKIndex1(Lekarz_ID),
  INDEX Badania_okulistyczne_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Badania_profilaktyczne (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Data_2 DATE NULL,
  Wiek int(11) UNSIGNED NULL,
  Dlugosc_dziaal int(11) UNSIGNED NULL,
  Obwod_glowy int(11) UNSIGNED NULL,
  Obwod_klatki_piersiowej int(11) UNSIGNED NULL,
  Ciemiaczko_przednie int(11) UNSIGNED NULL,
  Liczba_zebow int(11) UNSIGNED NULL,
  Zalecenia varchar(50) NULL,
  PRIMARY KEY(ID),
  INDEX Badania_profialaktyczne_FKIndex1(Lekarz_ID),
  INDEX Badania_profilaktyczne_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Badania_przesiewowe (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Nieprawidlowosc varchar(50) NULL,
  Data_2 DATE NULL,
  Wiek_mies int(11) UNSIGNED NULL,
  Wyniki_badan varchar(50) NULL,
  Zalecenia varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Badania_przesiewowe_FKIndex1(Lekarz_ID),
  INDEX Badania_przesiewowe_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Dziecko (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Dziecko_PESEL int(11) UNSIGNED NOT NULL,
  Rodzic_PESEL int(11) UNSIGNED NOT NULL,
  Nazwisko varchar(50) NOT NULL,
  Imie varchar(50) NOT NULL,
  Data_urodzenia DATE NOT NULL,
  Miejsce_urodzenia varchar(50) NOT NULL,
  Adres varchar(50) NOT NULL,
  Grupa_krwi varchar(50) NULL,
  PRIMARY KEY(PESEL),
  INDEX Dziecko_FKIndex1(Rodzic_PESEL),
  INDEX Dziecko_FKIndex2(Dziecko_PESEL)
);

CREATE TABLE Inne_badania (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Wynik varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Inne_badania_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Karta_rozwoju (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Miesiac int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  W_pozycji_na_brzuchu_unosi_glowe_chwiejnie BOOL NOT NULL,
  Skupia_wzrok_na_twarzy_badajacego_przez_chwile BOOL NULL,
  Wydaje_dzwieki_gardlowe BOOL NULL,
  Utrzymuje_glowe_przy_podciaganiu_do_pozycji_siedzacej BOOL NULL,
  sledzi_wzrokiem_osobe_poruszaja_sie BOOL NULL,
  Reaguj_mimika_na_kontakt_z_badanym BOOL NULL,
  W_pozycji_na_brzuchu_opiera_sie_na_przedramieniu BOOL NULL,
  Utrzymuje_glowe_prosto_w_pozycji_siedzacej BOOL NULL,
  Bawi_sie_rekami_odpowiada_usmiechem_na_usmiech BOOL NULL,
  W_pozycji_na_brzuchu_nogi_proste_lub_pol_wyprostowane BOOL NULL,
  Podciaganie_do_pozycji_siedzacej_unosi_glowe_i_ramiona BOOL NULL,
  W_odpowiedzi_smieje_sie_glosno_i_gaworzy BOOL NULL,
  W_pozycji_na_brzuchu_opiera_sie_na_dloniach BOOL NULL,
  Chwyta_grzechotke_lezaca_w_zasiegu_reki_i_obraca_nia BOOL NULL,
  Wydaje_okrzyki_radosci BOOL NULL,
  Odwraca_sie_z_plecow_na_brzuch_i_na_odwrot BOOL NULL,
  Trzymane_pionowo_utrzymuje_czesciowo_ciezar_ciala BOOL NULL,
  Siedzac_z_oparciem_chwyta_pewnie_grzechotke_jedna_reka BOOL NULL,
  Siedzi_przez_moment_bez_podtrzymywania BOOL NULL,
  Pelza_okreznie_i_do_tylu BOOL NULL,
  Gaworzy_wymawiajac_sylaby BOOL NULL,
  Siedzi_prawie_bez_podparcia_poczatki_samodzielnego_siadania BOOL NULL,
  Pelza_do_przodu BOOL NULL,
  Szuka_przedmiotu_ktory_upadl_patrzy_za_nim BOOL NULL,
  Stoi_postawione_przy_poreczy BOOL NULL,
  Trzymane_pod_pachy_wykonuje_ruchy_chodzenia_raczkuje BOOL NULL,
  Wymawia_pierwsze_slowa_dwusylabowe BOOL NULL,
  Staje_samo_chodzi_bokiem_trzymajac_sie_poreczy BOOL NULL,
  Wyjmuje_maly_przedmiot_z_duzego BOOL NULL,
  Nasladuje_takie_czynnosci_jak_kosi_kosi_pa_pa_itp BOOL NULL,
  Chodzi_trzymane_za_dwie_rece BOOL NULL,
  Staje_samodzielnie_sprawnie_trzymajac_sie_poreczy BOOL NULL,
  Na_prosbe_poparta_gestem_podaje_przedmiot_nie_wypuszczajac_go BOOL NULL,
  Chodzi_trzymane_za_jedna_reke BOOL NULL,
  Wklada_maly_przedmiot_do_duzego BOOL NULL,
  Wymawia_pierwsze_trzy_slowa_dwusylabowe BOOL NULL,
  Chodzi_samodzielnie_nie_przechodzi_juz_na_czworaki BOOL NULL,
  Podpiera_sie_rekami_wchodzac_po_schodkach BOOL NULL,
  Mowi_piec_slow_dwusylabowych  BOOL NULL,
  Wchodzi_po_schodach_trzymane_za_jedna_reke BOOL NULL,
  Sygnalizuje_w_dzien_o_swoich_potrzebach_fizjologicznych BOOL NULL,
  Mowi_przynajmniej_osiem_slow BOOL NULL,
  Zalecenia varchar(50) NULL,
  PRIMARY KEY(PESEL, Miesiac, Lekarz_ID),
  INDEX Karta_rozwoju_FKIndex1(Lekarz_ID),
  INDEX Karta_rozwoju_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Ksiazka (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Dziecko_PESEL int(11) UNSIGNED NOT NULL,
  Choroby_W_Rodzinie varchar(50) NULL,
  antyD varchar(50) NULL,
  AntygenHBs varchar(50) NULL,
  Szkodliwosc_Zawodowe_Srodowiskowe varchar(50) NULL,
  Uzywki varchar(50) NULL,
  Leki varchar(50) NULL,
  Miejsce_Urodzenia varchar(50) NULL,
  Tydzien_Ciazy int(11) UNSIGNED NULL,
  Iokres int(11) UNSIGNED NULL,
  IIokres int(11) UNSIGNED NULL,
  Pekniecie_Pecherzyka_Plodowego varchar(50) NULL,
  Liczba_Godzin int(11) UNSIGNED NULL,
  Plyn_owodzniowy varchar(50) NULL,
  Rodzaj_porodu varchar(50) NULL,
  Wskazania varchar(50) NULL,
  Zastosowano_znieczulenie varchar(50) NULL,
  Leki_w_czasie_I_i_II_okresu_porodu varchar(50) NULL,
  Opis_powiklan varchar(50) NULL,
  Masa_urodzenia int(11) UNSIGNED NULL,
  Obwod_glowu int(11) UNSIGNED NULL,
  Dlugosc int(11) UNSIGNED NULL,
  Obwod_klatki_piersiowej int(11) UNSIGNED NULL,
  Urazy_okoloporodowe varchar(50) NULL,
  Opis_nieprawidlowosci_wad_wrodzonych varchar(50) NULL,
  PRIMARY KEY(PESEL),
  INDEX Ksiazka_FKIndex1(Dziecko_PESEL)
);

CREATE TABLE Leczenie_Szpitalne (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Oddzial_idOddzial int(11) UNSIGNED NOT NULL,
  Od DATE NULL,
  Do DATE NULL,
  Rozpoznanie varchar(50) NULL,
  Zalecenia varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX LeczenieSzpitalne_FKIndex1(Oddzial_idOddzial),
  INDEX LeczenieSzpitalne_FKIndex2(Ksiazka_PESEL),
  INDEX LeczenieSzpitalne_FKIndex3(Lekarz_ID)
);

CREATE TABLE Lekarz (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Oddzial_idOddzial int(11) UNSIGNED NOT NULL,
  Imie varchar(50) NULL,
  Nazwisko varchar(50) NULL,
  PRIMARY KEY(ID),
  INDEX Lekarz_FKIndex1(Oddzial_idOddzial)
);

CREATE TABLE Odczulacze (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Data_podania DATE NULL,
  Rodzaj_preparatu varchar(50) NULL,
  Objawy_uboczne varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Odczulacze_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Oddzial (
  idOddzial int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Szpital_idSzpital int(11) UNSIGNED NOT NULL,
  Nazwa varchar(50) NULL,
  PRIMARY KEY(idOddzial),
  INDEX Oddzial_FKIndex1(Szpital_idSzpital)
);

CREATE TABLE Porady (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  ID int(11) UNSIGNED NULL,
  Data_2 DATE NULL,
  Wiek_mies int(11) UNSIGNED NULL,
  Zalecenia varchar(50) NULL,
  PRIMARY KEY(PESEL),
  INDEX Porady_FKIndex1(Lekarz_ID),
  INDEX Porady_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Przebyte_choroby_zakazne (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Nazwa_choroby varchar(50) NULL,
  Data_zachorowania DATE NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Przebyte_choroby_zakazne_FKIndex1(Lekarz_ID),
  INDEX Przebyte_choroby_zakazne_FKIndex2(Ksiazka_PESEL)
);

CREATE TABLE Rodzic (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Nazwisko varchar(50) NOT NULL,
  Imie varchar(50) NOT NULL,
  Data_urodzenia int(11) UNSIGNED NOT NULL,
  Wykszalcenie varchar(50) NULL,
  Zawod varchar(50) NULL,
  PRIMARY KEY(PESEL)
);

CREATE TABLE Szczepienia (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Szczepienie varchar(50) NULL,
  Data_szczepienia DATE NULL,
  Nazwa_szczepionki varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Szczepienia_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Szczepienia_Gruzlica (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Data_2 DATE NULL,
  Wynik_testu varchar(50) NULL,
  Ocena_blizny varchar(50) NULL,
  Szczepienie_CBG_data DATE NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Szczepieni_Gruzlica_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Szpital (
  idSzpital int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Nazwa varchar(50) NULL,
  Adres varchar(50) NULL,
  PRIMARY KEY(idSzpital)
);

CREATE TABLE Transfuzja_Krwi (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Data_2 DATE NULL,
  Rodzaj_preparatu varchar(50) NULL,
  Dawka int(11) UNSIGNED NULL,
  Objawy_uczulenia  varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Transfuzja_Krwi_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Uczulenia (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Leki varchar(50) NULL,
  Objawy varchar(50) NULL,
  Pokarmy varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Uczulenia_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Uwagi (
  PESEL int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  ID int(11) UNSIGNED NOT NULL,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Tekst varchar(50) NULL,
  PRIMARY KEY(PESEL, ID),
  INDEX Uwagi_FKIndex1(Ksiazka_PESEL)
);

CREATE TABLE Wizyty_patronazowe (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  Ksiazka_PESEL int(11) UNSIGNED NOT NULL,
  Lekarz_ID int(11) UNSIGNED NOT NULL,
  Data_2 DATE NULL,
  Stan_zdrowia_i_zalecenia varchar(50) NULL,
  PRIMARY KEY(ID),
  INDEX Wizyty_patronazowe_FKIndex1(Lekarz_ID),
  INDEX Wizyty_patronazowe_FKIndex2(Ksiazka_PESEL)
);


